package com.am.server.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.Writer;

/**
 * Created by mike on 29.03.2014.
 */

@Controller
public class OrangeController {

    @RequestMapping(value = "/get", method = RequestMethod.GET)
    @ResponseBody
    public String getHomePage() {
        return "some value for get request";
    }

    @RequestMapping(value = "/json", method = RequestMethod.GET)
    public String hello() {
        return "json";
    }

    public void testMethod() {
        //TODO
    }

}
